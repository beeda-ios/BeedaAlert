// 
// Beeda Driver
//
// Created by Rakibur Khan on 6/3/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import Foundation

public enum CustomAlertType {
    case success
    case warning
    case failure
}
