// 
// Beeda Driver
//
// Created by Rakibur Khan on 6/3/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import SwiftUI
import BeedaUtilities

@frozen public enum CustomAlertActionStyle {
    case cancel
    case destructive
    case base
    
    public var textColor: UIColor? {
        var color: UIColor? = nil
        
        switch self {
            case .base:
                color = .systemBackground
                
            case .cancel:
                color = .color163BDE
                
            case .destructive:
                color = .colorF82814
        }
        
        return color
    }
    
    public var textColorSwift: Color? {
        var color: Color? = nil
        
        switch self {
            case .base:
                if #available(iOS 15.0, *) {
                    color = Color(uiColor: .systemBackground)
                } else {
                    color = Color(UIColor.systemBackground)
                }
                
            case .cancel:
                color = .color163BDE
                
            case .destructive:
                color = .colorF82814
        }
        
        return color
    }
    
    public var backgroundColor: UIColor? {
        var color: UIColor? = nil
        
        switch self {
            case .base:
                color = .color163BDE
                
            case .cancel:
                color = .colorF1F3FF
                
            case .destructive:
                color = .colorFFD4D0
        }
        
        return color
    }
    
    public var backgroundColorSwift: Color {
        var color: Color = .accentColor
        
        switch self {
            case .base:
                color = .color163BDE
                
            case .cancel:
                color = .colorF1F3FF
                
            case .destructive:
                color = .colorFFD4D0
        }
        
        return color
    }
}
