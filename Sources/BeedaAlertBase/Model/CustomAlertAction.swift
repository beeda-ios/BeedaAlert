// 
// Beeda Driver
//
// Created by Rakibur Khan on 6/3/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import Foundation

public struct CustomAlertAction {
    public let title: String
    public let style: CustomAlertActionStyle
    public var action: (()->Void)? = nil
    
    public init(title: String, style: CustomAlertActionStyle, action: (() -> Void)? = nil) {
        self.title = title
        self.style = style
        self.action = action
    }
}
