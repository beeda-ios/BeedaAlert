// 
// 
//
// Created by Rakibur Khan on 11/5/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import SwiftUI
import BeedaAlertBase
import BeedaUtilities

//MARK: - Alert
public struct BeedaAlertView<CustomIcon>: View where CustomIcon: View {
    @State private var isShown = false
    private let icon: CustomIcon?
    private let title: LocalizedStringKey
    private let message: LocalizedStringKey?
    private var type: CustomAlertType?
    private let defaultAction: CustomAlertAction?
    private let cancelAction: CustomAlertAction?
    private let outsideTapDismiss: Bool
    private let buttonsHidden: Bool
    private var isCustom: Bool
    
    public init(title: LocalizedStringKey, message: LocalizedStringKey? = nil, defaultAction: CustomAlertAction? = nil, cancelAction: CustomAlertAction? = nil, outsideTapDismiss: Bool = true, buttonsHidden: Bool = false, @ViewBuilder icon: ()-> CustomIcon) {
        self.icon = icon()
        self.title = title
        self.message = message
        self.type = .none
        
        if let defaultAction = defaultAction {
            self.defaultAction = defaultAction
        } else {
            self.defaultAction = CustomAlertAction(title: "OK", style: .base)
        }
        
        self.cancelAction = cancelAction
        self.outsideTapDismiss = outsideTapDismiss
        self.buttonsHidden = buttonsHidden
        self.isCustom = true
    }
    
    public var body: some View {
        ZStack(alignment: .center) {
            if isShown {
                if #available(iOS 15.0, *) {
                    AlertBackground(outsideTapDismiss: outsideTapDismiss)
                        .zIndex(0)
                } else {
                    AlertBackgroundLegacy(outsideTapDismiss: outsideTapDismiss)
                        .zIndex(0)
                }
                
                VStack(spacing: 34) {
                    if isCustom {
                        AlertCustomBody(title: title, message: message, icon: AnyView(icon))
                    } else {
                        AlertDefaultBody(title: title, message: message, type: type ?? .warning)
                    }
                    
                    HStack(spacing: 32) {
                        //Cancel Button
                        if #available(iOS 15.0, *) {
                            BeedaAlertButton(action: cancelAction, style: .cancel)
                        } else {
                            BeedaAlertButtonLegacy(action: cancelAction, style: .cancel)
                        }
                        
                        if #available(iOS 15.0, *) {
                            BeedaAlertButton(action: defaultAction, style: .base)
                        } else {
                            BeedaAlertButtonLegacy(action: defaultAction, style: .base)
                        }
                    }
                }
                .zIndex(1)
                .padding(.horizontal, 40)
                .padding(.top, 32)
                .padding(.bottom, 25)
                .frame(minWidth: 328, minHeight: 250)
                .background(
                    RoundedRectangle(cornerRadius: 20)
                        .fill(Color(.systemBackground))
                )
                .transition(.asymmetric(insertion: .scale, removal: .scale))
            }
        }
        .animation(Animation.easeInOut(duration: 0.7))
        .onAppear {
            isShown = true
        }
    }
}

extension BeedaAlertView where  CustomIcon == AnyView {
    public init(title: LocalizedStringKey, message: LocalizedStringKey? = nil, type: CustomAlertType, defaultAction: CustomAlertAction? = nil, cancelAction: CustomAlertAction? = nil, outsideTapDismiss: Bool = true, buttonsHidden: Bool = false) {
        self.init(title: title, message: message, defaultAction: defaultAction, cancelAction: cancelAction, outsideTapDismiss: outsideTapDismiss, buttonsHidden: buttonsHidden) {
            AnyView(Image(""))
        }
        
        self.type = type
        self.isCustom = false
    }
}

@available(iOS 15.0, *)
fileprivate struct AlertBackground: View {
    @Environment(\.dismiss) var dismiss
    let outsideTapDismiss: Bool
    
    var body: some View {
        Color(UIColor.label.withAlphaComponent(0.7))
            .onTapGesture {
                if outsideTapDismiss {
                    dismiss()
                }
            }
    }
}

fileprivate struct AlertBackgroundLegacy: View {
    @Environment(\.presentationMode) var presentationMode
    let outsideTapDismiss: Bool
    
    var body: some View {
        Color(UIColor.label.withAlphaComponent(0.7))
            .onTapGesture {
                if outsideTapDismiss {
                    presentationMode.wrappedValue.dismiss()
                }
            }
    }
}

fileprivate struct AlertDefaultBody: View {
    let title: LocalizedStringKey
    let message: LocalizedStringKey?
    let type: CustomAlertType
    
    var body: some View {
        VStack(spacing: 28) {
            AlertIconDefault(type: type)
                .aspectRatio(1, contentMode: .fit)
                .frame(maxWidth: 80)
            
            AlertTexts(title: title, message: message)
                .layoutPriority(3)
        }
    }
}

//MARK: - Alert Custom Body
fileprivate struct AlertCustomBody: View {
    let title: LocalizedStringKey
    let message: LocalizedStringKey?
    let icon: AnyView?
    
    var body: some View {
        VStack(spacing: 28) {
            if let icon = icon {
                icon
                    .aspectRatio(1, contentMode: .fit)
                    .frame(maxWidth: 80)
            }
            
            AlertTexts(title: title, message: message)
                .layoutPriority(3)
        }
    }
}

//MARK: - Alert Default icon
fileprivate struct AlertIconDefault: View {
    let type: CustomAlertType?
    
    var body: some View {
        switch type {
            case .success:
                return AnyView(
                    LottieView(name: "success")
                        .scaledToFit()
                )
                
            case .warning:
                return AnyView(
                    Image(systemName: "exclamationmark.triangle.fill")
                        .resizable()
                        .foregroundColor(.colorFFB800)
                        .scaledToFit()
                )
                
            case .failure:
                return AnyView(
                    Image(systemName: "xmark.circle")
                        .resizable()
                        .foregroundColor(.red)
                        .scaledToFit()
                )
                
            default:
                return AnyView(EmptyView())
                
        }
    }
}

//MARK: - Alert title and subtitle
fileprivate struct AlertTexts: View {
    let title: LocalizedStringKey
    let message: LocalizedStringKey?
    
    var body: some View {
        VStack(alignment: .center, spacing: 8) {
            if let message = message {
                Text(title, comment: "AlertTitle")
                    .font(.poppins(forTextStyle: .headline, type: .medium, fontSize: 20))
                    .foregroundColor(.color2E3A59)
                    .multilineTextAlignment(.center)
                    .layoutPriority(4)
                
                Text(message, comment: "AlertBody")
                    .font(.poppins(forTextStyle: .subheadline, type: .regular, fontSize: 14))
                    .foregroundColor(.color979797)
                    .multilineTextAlignment(.center)
                    .layoutPriority(3)
            } else {
                Text(title, comment: "AlertTitle")
                    .font(.poppins(forTextStyle: .subheadline, type: .medium, fontSize: 16))
                    .foregroundColor(.color2E3A59)
                    .multilineTextAlignment(.center)
                    .layoutPriority(4)
            }
        }
    }
}

fileprivate struct BeedaAlertButtonLegacy: View {
    @Environment(\.presentationMode) private var presentationMode
    
    let action: CustomAlertAction?
    let style: CustomAlertActionStyle
    
    var body: some View {
        Button {
            if let action = action?.action {
                action()
            }
            
            presentationMode.wrappedValue.dismiss()
        } label: {
            Text(action?.title ?? "Cancel")
        }
        .buttonStyle(BeedaAlertButtonStyle(role: action?.style ?? style))
    }
}

@available (iOS 15.0, *)
fileprivate struct BeedaAlertButton: View {
    @Environment(\.dismiss) private var dismiss
    
    let action: CustomAlertAction?
    let style: CustomAlertActionStyle
    
    var body: some View {
        Button {
            if let action = action?.action {
                action()
            }
            
            dismiss()
        } label: {
            Text(action?.title ?? "Cancel")
        }
        .buttonStyle(BeedaAlertButtonStyle(role: action?.style ?? style))
    }
}

struct BeedaAlertView_Previews: PreviewProvider {
    static var previews: some View {
        BeedaAlertView(title: "This is a preview of alert title", message: "This alert body shows that any alert can be placed and the size and lines will be dynamic", type: .success)
        
        BeedaAlertView(title: "This is a preview of alert title", message: "This alert body shows that any alert can be placed and the size and lines will be dynamic") {
            Image(systemName: "trash.circle.fill")
                .resizable()
                .foregroundColor(.red)
        }
    }
}
