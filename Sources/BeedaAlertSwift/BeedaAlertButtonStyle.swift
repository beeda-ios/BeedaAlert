// 
//
// Created by Rakibur Khan on 26/4/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import SwiftUI
import BeedaAlertBase

struct BeedaAlertButtonStyle: ButtonStyle {
    private var style: CustomAlertActionStyle
    
    init(role: CustomAlertActionStyle = .base) {
        style = role
    }
    
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .font(.poppins(forTextStyle: .callout, type: .medium, fontSize: 16))
            .foregroundColor(style.textColorSwift ?? .accentColor)
            .padding(.horizontal, 24)
            .padding(.vertical, 8)
            .frame(minWidth: 112)
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .fill(configuration.isPressed ? style.backgroundColorSwift.opacity(0.8) : style.backgroundColorSwift)
            )
            .scaleEffect(configuration.isPressed ? 0.9 : 1)
            .animation(.easeOut(duration: 0.2), value: configuration.isPressed)
    }
}

struct BeedaButtonStyle_Previews: PreviewProvider {
    static var previews: some View {
        Button{} label: {
            Text("Beeda Button")
        }
        .buttonStyle(BeedaAlertButtonStyle(role: .cancel))
    }
}

