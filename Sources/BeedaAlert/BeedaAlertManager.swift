// 
// 
//
// Created by Rakibur Khan on 9/5/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import UIKit
import BeedaAlertBase

public class BeedaAlertManager: NSObject {
    public static let shared = BeedaAlertManager(window: nil)
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var overlayView = UIView()
    var mainView = UIView()
    var titleLabel = UILabel()
    var imgView = UIImageView()
    
    private let alertViewTag: Int = 25378
    
    public var window: UIWindow?
    
    public var statusBarHeight: CGFloat? {
        window?.windowScene?.statusBarManager?.statusBarFrame.height
    }
    
    public init(window: UIWindow?){
        super.init()
        
        self.window = window
    }
    
    @MainActor
    public func updateWindow(_ windoew: UIWindow) {
        self.window = windoew
    }
    
    @MainActor
    public func showLoading(color: UIColor = UIColor.systemBackground) {
        if !activityIndicator.isAnimating {
            self.mainView = UIView()
            self.mainView.frame = UIScreen.main.bounds
            self.mainView.backgroundColor = .clear
            self.overlayView = UIView()
            self.imgView = UIImageView()
            self.activityIndicator = UIActivityIndicatorView()
            
            overlayView.frame = UIScreen.main.bounds
            overlayView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            overlayView.clipsToBounds = true
            overlayView.layer.zPosition = 1
            
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            activityIndicator.center = CGPoint(x: overlayView.bounds.width / 2, y: overlayView.bounds.height / 2)
            activityIndicator.style = .medium
            
            overlayView.addSubview(activityIndicator)
            self.mainView.addSubview(overlayView)
            
            if window?.viewWithTag(101) != nil {
                
            } else {
                overlayView.center = window?.center ?? CGPoint(x: 0, y: 0)
                mainView.tag = 101
                window?.addSubview(mainView)
                activityIndicator.startAnimating()
            }
        }
    }
    
    @MainActor
    public func hideLoading() {
        activityIndicator.stopAnimating()
        window?.viewWithTag(101)?.removeFromSuperview()
    }
    
    //MARK: Set view for hiden
    @MainActor
    public func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.2, options: .transitionCrossDissolve) {
            view.isHidden = hidden
        }
    }
}

extension BeedaAlertManager {
    @MainActor
    public func showAlert(title: String, message: String? = nil, type: CustomAlertType, defaultAction: CustomAlertAction? = nil, cancelAction: CustomAlertAction? = nil, outsideTapDismiss: Bool = true, customTag: Int? = nil) {
        if let window = window {
            let alert = BeedaAlertView(frame: window.frame)
            
            if let tag = customTag {
                alert.tag = tag
            } else {
                alert.tag = alertViewTag
            }
            
            alert.setupData(title: title, message: message, type: type,  defaultAction: defaultAction, cancelAction: cancelAction, outsideTapDismiss: outsideTapDismiss, buttonsHidden: false)
            alert.setupView()
            alert.layoutIfNeeded()
            window.addSubview(alert)
        }
    }
    
    @MainActor
    public func showAlert(icon: UIImage?, title: String, message: String? = nil, defaultAction: CustomAlertAction? = nil, cancelAction: CustomAlertAction? = nil, outsideTapDismiss: Bool = true, customTag: Int? = nil) {
        if let window = window {
            let alert = BeedaAlertView(frame: window.frame)
            
            if let tag = customTag {
                alert.tag = tag
            } else {
                alert.tag = alertViewTag
            }
            
            alert.setupData(icon: icon, title: title, message: message, defaultAction: defaultAction, cancelAction: cancelAction, outsideTapDismiss: outsideTapDismiss, buttonsHidden: false)
            alert.setupView()
            alert.layoutIfNeeded()
            window.addSubview(alert)
        }
    }
    
    @MainActor
    public func removeAlert(customTag: Int? = nil){
        if let window = window  {
            if let tag = customTag {
                if let view = window.viewWithTag(tag){
                    view.removeFromSuperview()
                }
            } else {
                if let view = window.viewWithTag(alertViewTag){
                    view.removeFromSuperview()
                }
            }
        }
    }
}
