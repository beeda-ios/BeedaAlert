// 
// 
//
// Created by Rakibur Khan on 6/3/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import UIKit
import Lottie
import BeedaUtilities
import BeedaAlertBase

class BeedaAlertView: UIView {
    @IBOutlet private weak var alertBaseView: UIView!
    @IBOutlet private weak var alertBlurView: UIView!
    @IBOutlet private weak var alertIconView: UIView!
    @IBOutlet private weak var alertTitle: UILabel!
    @IBOutlet private weak var alertSubtitle: UILabel!
    
    @IBOutlet private weak var buttonsBackground: UIView!
    @IBOutlet private weak var successButton: UIButton!
    @IBOutlet private weak var failureButton: UIButton!
    
    private var icon: UIImage?
    private var titleText: String?
    private var subtitleText: String?
    private var type: CustomAlertType?
    private var defaultAction: CustomAlertAction?
    private var cancelAction: CustomAlertAction?
    private var disableDismissOnTap: Bool = false
    private var buttonsHidden: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        commonInit()
    }
    
    private func commonInit() {
        loadViewFromNib(bundle: .module)
        setViews()
        setupTapGuesture()
    }
    
    private func setViews() {
        alertBlurView.backgroundColor = UIColor.label.withAlphaComponent(0.8)
        alertBaseView.roundedCorner(radius: 20)
        alertTitle.font = .poppins(forTextStyle: .headline, type: .medium, fontSize: 20)
        alertSubtitle.font = .poppins(forTextStyle: .subheadline, type: .regular, fontSize: 14)
        alertTitle.textColor = .color2E3A59
        alertSubtitle.textColor = .color979797
    }
    
    private func setupTapGuesture() {
        alertBlurView.isUserInteractionEnabled = true
        let tapGuesture = UITapGestureRecognizer(target: self, action: #selector(blurViewTapped))
        alertBlurView.addGestureRecognizer(tapGuesture)
    }
    
    @objc private func blurViewTapped() {
        if !disableDismissOnTap {
            dismiss(animated: true)
        }
    }
    
    //MARK: - Button Actions
    @IBAction private func successAction(_ sender: UIButton) {
        if let action = defaultAction?.action {
            action()
        }
        
        dismiss(animated: true)
    }
    
    @IBAction private func failureAction(_ sender: UIButton) {
        if let action = cancelAction?.action {
            action()
        }
        
        dismiss(animated: true)
    }
    
    private func dismiss(animated: Bool) {
        if animated {
            UIView.animate(withDuration: TimeInterval(0.7)) {
                self.removeFromSuperview()
            }
        } else {
            self.removeFromSuperview()
        }
    }
}

//MARK: - Data Setup
extension BeedaAlertView {
    func setupData(title: String, message: String? = nil, type: CustomAlertType = .success, defaultAction: CustomAlertAction? = nil, cancelAction: CustomAlertAction? = nil, outsideTapDismiss: Bool = true, buttonsHidden: Bool = false) {
        titleText = title
        subtitleText = message
        self.type = type
        self.defaultAction = defaultAction
        self.cancelAction = cancelAction
        disableDismissOnTap = !outsideTapDismiss
        self.buttonsHidden = buttonsHidden
    }
    
    func setupData(icon: UIImage?, title: String, message: String? = nil, defaultAction: CustomAlertAction? = nil, cancelAction: CustomAlertAction? = nil, outsideTapDismiss: Bool = true, buttonsHidden: Bool = false) {
        titleText = title
        subtitleText = message
        self.icon = icon
        self.type = nil
        self.defaultAction = defaultAction
        self.cancelAction = cancelAction
        disableDismissOnTap = !outsideTapDismiss
        self.buttonsHidden = buttonsHidden
    }
    
    func setupData(animationJSON: String, title: String, message: String? = nil, defaultAction: CustomAlertAction? = nil, cancelAction: CustomAlertAction? = nil, outsideTapDismiss: Bool = true, buttonsHidden: Bool = false) {
        titleText = title
        subtitleText = message
        self.defaultAction = defaultAction
        self.cancelAction = cancelAction
        disableDismissOnTap = !outsideTapDismiss
        self.buttonsHidden = buttonsHidden
    }
    
    func setupView() {
        if buttonsHidden {
            buttonsBackground.isHidden = true
        } else {
            buttonsBackground.isHidden = false
            
            if let baseAction = defaultAction {
                customizeButton(button: successButton, title: baseAction.title, type: baseAction.style)
            } else {
                customizeButton(button: successButton, title: NSLocalizedString("OK", comment: "Button: OK"), type: .base)
            }
            
            if let cancelAction = cancelAction {
                failureButton.isHidden = false
                customizeButton(button: failureButton, title: cancelAction.title, type: cancelAction.style)
            } else {
                failureButton.isHidden = true
            }
        }
        
        alertTitle.text = titleText
        alertSubtitle.text = subtitleText
        
        if subtitleText == nil {
            alertTitle.font = .poppins(forTextStyle: .subheadline, type: .medium, fontSize: 16)
        } else {
            alertTitle.font = .poppins(forTextStyle: .headline, type: .medium, fontSize: 20)
        }
        
        if let type = type {
            switch type {
                case .success:
                    setupSuccessIcon()
                case .warning:
                    setupWarningIcon()
                case .failure:
                    setupFailureIcon()
            }
        } else {
            setupCustomIcon()
        }
    }
}

//MARK: - UI modification based on data
extension BeedaAlertView {
    private func setupSuccessIcon() {
        let animation = LottieAnimation.named("success", bundle: .module)
        let animationView = LottieAnimationView(animation: animation)
        animationView.animationSpeed = 1
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.loopMode = .playOnce
        animationView.contentMode = .scaleToFill
        
        setAlertIconConstraints(with: animationView)
        
        animationView.play()
    }
    
    private func setupWarningIcon() {
        let image = UIImage(systemName: "exclamationmark.triangle.fill")
        let imageView = UIImageView(image: image)
        imageView.tintColor = .colorFFB800
        imageView.contentMode = .scaleAspectFit
        
        setAlertIconConstraints(with: imageView)
    }
    
    private func setupFailureIcon() {
        let image = UIImage(systemName: "xmark.circle")
        let imageView = UIImageView(image: image)
        imageView.tintColor = .systemRed
        imageView.contentMode = .scaleAspectFit
        
        setAlertIconConstraints(with: imageView)
    }
    
    private func setupCustomIcon() {
        let imageView = UIImageView(image: icon)
        imageView.contentMode = .scaleAspectFit
        
        setAlertIconConstraints(with: imageView)
    }
    
    private func setAlertIconConstraints(with iconView: UIView) {
        alertIconView.subviews.forEach{$0.removeFromSuperview()}
        
        alertIconView.addSubview(iconView)
        iconView.translatesAutoresizingMaskIntoConstraints = false
        let leading = iconView.leadingAnchor.constraint(equalTo: alertIconView.leadingAnchor, constant: 0)
        let trailing = iconView.trailingAnchor.constraint(equalTo: alertIconView.trailingAnchor, constant: 0)
        let top = iconView.topAnchor.constraint(equalTo: alertIconView.topAnchor, constant: 0)
        let bottom = iconView.bottomAnchor.constraint(equalTo: alertIconView.bottomAnchor, constant: 0)
        let constraints = [leading, trailing, top, bottom]
        NSLayoutConstraint.activate(constraints)
        
        alertIconView.bringSubviewToFront(iconView)
    }
}
    
//MARK: - UI Customization
extension BeedaAlertView {
    private func customizeButton(button: UIButton, title: String, type: CustomAlertActionStyle) {
        let buttonFont: UIFont = .poppins(forTextStyle: .callout, type: .medium, fontSize: 16)
        button.setTitle(title, for: .normal)
        button.roundedCorner(radius: 8)
        
        if #available(iOS 15, *) {
            var configuration = UIButton.Configuration.filled()
            
            configuration.title = title
            
            configuration.titleTextAttributesTransformer = UIConfigurationTextAttributesTransformer { incoming in
                var outgoing = incoming
                outgoing.font = buttonFont
                
                return outgoing
            }
            
            configuration.contentInsets = NSDirectionalEdgeInsets(top: 7, leading: 10, bottom: 7, trailing: 10)
            
            configuration.baseBackgroundColor = type.backgroundColor
            configuration.baseForegroundColor = type.textColor
            
            button.configuration = configuration
            button.updateConfiguration()
            
        } else {
            button.setTitleColor(type.textColor, for: .normal)
            button.backgroundColor = type.backgroundColor
            
            button.titleLabel?.font = buttonFont
            button.contentEdgeInsets = UIEdgeInsets(top: 7, left: 10, bottom: 7, right: 10)
        }
        
        switch type {
            case .cancel:
                button.setBorder(width: 0.5, color: type.textColor)
                
            default:
                break
        }
    }
}
