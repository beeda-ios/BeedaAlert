// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "BeedaAlert",
    platforms: [.iOS(.v13)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "BeedaAlert",
            targets: ["BeedaAlert"]),
        .library(
            name: "BeedaAlertSwift",
            targets: ["BeedaAlertSwift"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(url: "https://lab.easital.com/beeda-platform/beeda-ios/BeedaUtilities.git", from: Version(1, 0, 0)),
        .package(url: "https://github.com/airbnb/lottie-ios.git", from: Version(4,0,0))
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "BeedaAlert",
            dependencies: [.product(name: "Lottie", package: "lottie-ios"), "BeedaUtilities", "BeedaAlertBase"],
            resources: [.process("Resources/Fonts"), .process("Resources/JSON")]),
        .target(
            name: "BeedaAlertSwift",
            dependencies: [.product(name: "Lottie", package: "lottie-ios"), "BeedaUtilities", "BeedaAlertBase"],
            resources: [.process("Resources/Fonts"), .process("Resources/JSON")]),
        .target(
            name: "BeedaAlertBase",
            dependencies: ["BeedaUtilities"]),
        .testTarget(
            name: "BeedaAlertTests",
            dependencies: ["BeedaAlert", "BeedaAlertSwift"]),
    ]
)
